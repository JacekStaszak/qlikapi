﻿using Newtonsoft.Json;

namespace QlikSenseAPI
{
    public class LinkedSourceItems
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("datasourceID")]
        public string Source { get; set; }
        [JsonProperty("qConnectStatement")]
        public string ConnectStatement { get; set; }
        [JsonProperty("qName")]
        public string ConnectionName { get; set; }


    }

    public class LinkedSources
    {
        [JsonProperty("data")]
        public List<LinkedSourceItems> Data { get; set; }
    }
}
