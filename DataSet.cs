﻿using Newtonsoft.Json;

namespace QlikSenseAPI
{
    public class DataSetResourceAttributes
    {
        [JsonProperty("dataStoreType")]
        public string DataStoreType { get; set; }

        [JsonProperty("sourceSystemId")]
        public string SourceSystemId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("dataStoreName")]
        public string DataStoreName { get; set; }
    }

    public class DataSetList
    {
        [JsonProperty("data")]
        public List<DataSetItem> Data { get; set; }
    }



    public class DataSetItem
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("resourceAttributes")]
        public DataSetResourceAttributes ResourceAttributes { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("creatorId")]
        public string CreatorId { get; set; }

        [JsonProperty("updaterId")]
        public string UpdaterId { get; set; }

        [JsonProperty("tenantId")]
        public string TenantId { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

    }
}
