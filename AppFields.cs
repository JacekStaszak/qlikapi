﻿namespace QlikSenseAPI
{
    public class AppField
    {
        public string Name { get; set; }
        public List<string> SrcTables { get; set; }
        public bool IsSystem { get; set; }
        public bool IsHidden { get; set; }
        public bool IsSemantic { get; set; }
        public bool DistinctOnly { get; set; }
        public int Cardinal { get; set; }
        public int TotalCount { get; set; }
        public bool IsLocked { get; set; }
        public bool AlwaysOneSelected { get; set; }
        public bool IsNumeric { get; set; }
        public string Comment { get; set; }
        public List<string> Tags { get; set; }
        public int ByteSize { get; set; }
        public string Hash { get; set; }
    }

    public class Table
    {
        public string Name { get; set; }
        public bool IsSystem { get; set; }
        public bool IsSemantic { get; set; }
        public bool IsLoose { get; set; }
        public int NoOfRows { get; set; }
        public int NoOfFields { get; set; }
        public int NoOfKeyFields { get; set; }
        public string Comment { get; set; }
        public int ByteSize { get; set; }
    }

    public class ReloadMeta
    {
        public int CpuTimeSpentMs { get; set; }
        public Hardware Hardware { get; set; }
        public int PeakMemoryBytes { get; set; }
    }

    public class Hardware
    {
        public int LogicalCores { get; set; }
        public long TotalMemory { get; set; }
    }

    public class AppMetadata
    {
        public ReloadMeta ReloadMeta { get; set; }
        public int StaticByteSize { get; set; }
        public List<AppField> Fields { get; set; }
        public List<Table> Tables { get; set; }
        public bool HasSectionAccess { get; set; }
        public List<object> TablesProfilingData { get; set; }
        public bool IsDirectQueryMode { get; set; }
        public string Usage { get; set; }
    }
}
