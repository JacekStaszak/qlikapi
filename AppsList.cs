﻿using Newtonsoft.Json;

namespace QlikSenseAPI
{
    public class AppResourceAttributes
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("spaceId")]
        public string SpaceId { get; set; }
    }

    public class AppsList
    {
        [JsonProperty("data")]
        public List<AppItem> Data { get; set; }
    }

    public class Links
    {
        [JsonProperty("self")]
        public Link Self { get; set; }
    }

    public class Link
    {
        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public class AppItem
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("resourceAttributes")]
        public AppResourceAttributes ResourceAttributes { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("creatorId")]
        public string CreatorId { get; set; }

        [JsonProperty("updaterId")]
        public string UpdaterId { get; set; }

        [JsonProperty("tenantId")]
        public string TenantId { get; set; }

        [JsonProperty("links")]
        public Links Links { get; set; }
    }
}
