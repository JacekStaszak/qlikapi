﻿using Newtonsoft.Json;
using QlikSenseAPI;
using System.Net.Http.Headers;

class Program


{
    static async Task Main()
    {
        string ApiKey = "PLACEHOLDER";
        string apiUrl = "https://QLIKINSTANCE.eu.qlikcloud.com/api/v1/";


        Console.WriteLine("apps");
        GetAppsAsync(ApiKey, apiUrl, "items?resourceType=app").Wait();
        Console.WriteLine("fields");
        GetAppFields(ApiKey, apiUrl, "201adc28-8a9e-4257-a3c6-6926ec155166", "/data/metadata").Wait();
        Console.WriteLine("Datasets");
        GetDataSets(ApiKey, apiUrl, "items?resourceType=dataset").Wait();
        Console.WriteLine("LinkedSources");
        GetLinkedSources(ApiKey, apiUrl, "data-connections").Wait();

    }

    public static async Task GetDataSets(string ApiKey, string apiUrl, string search_item)
    {



        using (var httpClient = new HttpClient())
        {

            // Set the Authorization header with the Bearer token
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ApiKey);
            try
            {
                // Make the GET request to the API
                HttpResponseMessage response = await httpClient.GetAsync(apiUrl + search_item);

                // Check if the request was successful (status code 200 OK)
                if (response.IsSuccessStatusCode)
                {
                    // Read and output the response content

                    string responseBody = await response.Content.ReadAsStringAsync();
                    DataSetList dataset_data = JsonConvert.DeserializeObject<DataSetList>(responseBody);

                    foreach (var item in dataset_data.Data)
                    {
                        Console.WriteLine(item.ResourceAttributes.Type);
                    }


                }
                else
                {
                    Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
            }
        }
    }

    public static async Task GetAppsAsync(string ApiKey, string apiUrl, string search_item)
    {
        using (var httpClient = new HttpClient())
        {
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ApiKey);

            try
            {
                // Make the GET request to the API
                HttpResponseMessage response = await httpClient.GetAsync(apiUrl + search_item);

                // Check if the request was successful (status code 200 OK)
                if (response.IsSuccessStatusCode)
                {
                    // Read and output the response content

                    string responseBody = await response.Content.ReadAsStringAsync();
                    AppsList app_data = JsonConvert.DeserializeObject<AppsList>(responseBody);

                    foreach (var item in app_data.Data)
                    {
                        Console.WriteLine(item.ResourceAttributes.Id);
                    }
                }
                else
                {
                    Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
            }
        }
    }



    public static async Task GetLinkedSources(string ApiKey, string apiUrl, string search_item)
    {
        using (var httpClient = new HttpClient())
        {

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ApiKey);
            try
            {
                // Make the GET request to the API
                HttpResponseMessage response = await httpClient.GetAsync(apiUrl + search_item);

                // Check if the request was successful (status code 200 OK)
                if (response.IsSuccessStatusCode)
                {
                    // Read and output the response content

                    string responseBody = await response.Content.ReadAsStringAsync();
                    LinkedSources sources_data = JsonConvert.DeserializeObject<LinkedSources>(responseBody);
                    foreach (var item in sources_data.Data)
                    {
                        Console.WriteLine(item.ConnectionName);
                    }


                }
                else
                {
                    Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
            }
        }
    }


    public static async Task GetAppFields(string ApiKey, string apiUrl, string appId, string search_item)
    {
        using (var httpClient = new HttpClient())
        {
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ApiKey);

            try
            {
                // Make the GET request to the API
                HttpResponseMessage response = await httpClient.GetAsync(apiUrl + "apps/" + appId + search_item);

                // Check if the request was successful (status code 200 OK)
                if (response.IsSuccessStatusCode)
                {
                    // Read and output the response content

                    string responseBody = await response.Content.ReadAsStringAsync();
                    AppMetadata metadata_data = JsonConvert.DeserializeObject<AppMetadata>(responseBody);

                    foreach (var item in metadata_data.Fields)
                    {

                        Console.WriteLine(item.Name);
                    }



                }
                else
                {
                    Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
            }
        }
    }
}
